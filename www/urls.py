"""www URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.todolist_overview, name="overview"),
    url(r'^overview/$', views.todolist_overview, name="todolist_overview"),
    url(r'^add/$', views.todolist_add, name="todolist_add"),
    url(r'^delete/(?P<object_id>\d+)/$', views.todolist_delete, name="todolist_delete"),
    url(r'^edit/(?P<object_id>\d+)/$', views.todolist_edit, name="todolist_edit"),
    url(r'^done/(?P<object_id>\d+)/$', views.todolist_done, name="todolist_done"),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
]
