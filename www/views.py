from django.shortcuts import render,HttpResponse, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from www.todolist.models import TodoEntry
from www.todolist.forms import EntryForm
from django.template.context_processors import csrf

# index view (just redirect to login page)
def index(request):
  return HttpResponseRedirect('/login/')

########################################################################################
def todolist_overview(request):
	
	if request.user.id is None:
		return HttpResponseRedirect('/login/')
	open_list = TodoEntry.objects.filter(User = request.user.id, Status='OPEN')
	done_list = TodoEntry.objects.filter(User = request.user.id, Status='DONE')
	if (len(open_list) + len(done_list) > 0):
		percentage = int(100*len(open_list)/(len(open_list)+len(done_list)))
	else:
		percentage = 0
	percentage2= 100-percentage

	return render_to_response("todolist_overview", {"open_list": open_list, "done_list": done_list, "username":request.user.username, "percentage":percentage, "percentage2":percentage2, "opencount": len(open_list), "donecount": len(done_list)})
########################################################################################
def todolist_add(request):
	if request.user.id is  None:
		return HttpResponseRedirect('/login/')
	form = EntryForm(request.POST or None)

	if form.is_valid():
		add_object = TodoEntry()
		add_object.User = request.user
		add_object.Title = form.cleaned_data['Title']
		add_object.Description = form.cleaned_data['Description']
		add_object.Duedate = form.cleaned_data['Duedate']
		add_object.Duetime = form.cleaned_data['Duetime']
		add_object.save()
		return HttpResponseRedirect('/')
	
	return render_to_response("form.html",{"form": form}, context_instance=RequestContext(request))

########################################################################################
def todolist_edit(request, object_id):
	if request.user.id is  None:
		return HttpResponseRedirect('/login/')
	username = request.user
	form = EntryForm(request.POST or None)

	try:
		edit_object = TodoEntry.objects.get(id=object_id)
	except ObjectDoesNotExist, e:
		raise Http404
	print object_id

	if edit_object.User.id == request.user.id:
		if form.is_valid():
			edit_object.User = request.user
			edit_object.Title = form.cleaned_data['Title']
			edit_object.Description = form.cleaned_data['Description']
			edit_object.Duedate = form.cleaned_data['Duedate']
			edit_object.Duetime = form.cleaned_data['Duetime']
			edit_object.save()
			return HttpResponseRedirect('/')
		else:
			form = EntryForm(initial={'Title': edit_object.Title, 'Description':edit_object.Description, 'Duedate':edit_object.Duedate, 'Duetime':edit_object.Duetime}) 
			return render_to_response("form.html",{"form": form, "object_id":object_id, "username":username}, context_instance=RequestContext(request))

	else:
		raise Http404
########################################################################################
def todolist_delete(request, object_id):
	if request.user.id is  None:
		#return HttpResponseNotFound('<h2>Please Login</h2>')
		return HttpResponseRedirect('/login/')

	try:
		del_object = TodoEntry.objects.get(id=object_id)
	except ObjectDoesNotExist, e:
		raise Http404

	if del_object.User.id == request.user.id:
		del_object.delete()

	return HttpResponseRedirect('/')

########################################################################################
def todolist_done(request, object_id):
	if request.user.id is  None:
		#return HttpResponseNotFound('<h2>Please Login</h2>')
		return HttpResponseRedirect('/login/')

	try:
		sw_object = TodoEntry.objects.get(id = object_id)
	except ObjectDoesNotExist, e:
		raise Http404

	if sw_object.User.id == request.user.id:
		if sw_object.Status == 'OPEN':
			sw_object.Status='DONE'
		else:
			sw_object.Status='OPEN'
		sw_object.save()

	return HttpResponseRedirect('/')


