from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse, resolve
from django.contrib.auth.models import User
from www.todolist.models import TodoEntry
# default settings for todolist-tests
class TodoTestCase(TestCase):
    fixtures = ["initial.json"] 

# default settings for todolist-tests
class TodoTestCase(TestCase):
    def setUp(self):
        # Insertion of a second user
        user = 'test'
        password = '1234'
        self.user1 = User.objects.get(pk=1)
        self.user2 = User.objects.create_user(
            username=user, password=password)
        # Login of the new inserted user as default user for all tests
        self.client.login(username=user, password=password)


# testing the correct insertion of the testdata
class FixtureTests(TodoTestCase):
    def test_entries(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.assertEqual(User.objects.all().count(), 2)
        
        self.assertEqual(User.objects.get(pk=1), self.user1)
    
        self.assertEqual(User.objects.get(pk=2), self.user2)
        print("FixtureTests and Usercreation executed.")


# Tests to change and insert Data
class Inserts(TodoTestCase):

    def test_data_change(self):
        s = TodoEntry.objects.get(pk=3)
        self.assertEquals(s.id, 3)
        s.Title = 'Erster Eintrag'
        s.save()
        self.assertEquals(s.Title, 'Erster Eintrag')

    def test_data_insert(self):
        x = TodoEntry.objects.all().count() + 1
        s = TodoEntry.objects.create(
            Title='XYZ',
            Description='XYZ-Description',
            Duedate='2015-06-19',
            Status='DONE',
            User_id=1,)
        s.save()
        self.assertEqual(TodoEntry.objects.all().count(), x)

    def test_invalidData(self):
        self.client = Client()
        data = {"Description": "too less data"}
        response = self.client.post(reverse("todolist_add"), data=data)
        self.assertEqual(response.status_code, 302)
        print("InsertTests executed.")



# Testing urls resolving to appropriate views
class UrlTests(TodoTestCase):
    
    def test_todolist_url_overview(self):
        resolver = resolve('/')
        self.assertEqual(resolver.view_name, 'overview')

    def test_todolist_url_add(self):
        resolver = resolve('/add/')
        self.assertEqual(resolver.view_name, 'todolist_add')

    def test_todolist_url_edit(self):
        resolver = resolve('/edit/1/')
        self.assertEqual(resolver.view_name, 'todolist_edit')

    def test_todolist_url_switch(self):
        resolver = resolve('/done/1/')
        self.assertEqual(resolver.view_name, 'todolist_done')

    def test_todolist_url_delete(self):
        resolver = resolve('/delete/1/')
        self.assertEqual(resolver.view_name, 'todolist_delete')
        print("URL Tests executed")


# testing all views with valid user and without valid user
class UserViewTest(TodoTestCase):
    def test_overviewView(self):
        self.client.login(username='user', password='1234')
        request = self.client.get(reverse('todolist_overview'))
        self.assertEqual(request.status_code, 200)
        self.assertTemplateUsed(request, 'todolist_overview')
        self.assertContains(request, 'Eintrag', count=4)
        self.client.logout()
        request = self.client.get(reverse('todolist_overview'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_addView_get(self):
        request = self.client.get(reverse('todolist_add'))
        self.assertEqual(request.status_code, 200)
        self.assertTemplateUsed(request, 'form.html')
        self.client.logout()
        request = self.client.get(reverse('todolist_add'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_addView_post(self):
        response = self.client.post(reverse('todolist_add'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form.html')
        self.client.logout()
        response = self.client.post(reverse('todolist_add'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_editView_get(self):
        request = self.client.get(reverse('todolist_edit', args={1}))
        self.assertEqual(request.status_code, 404)
        self.assertTemplateUsed(request, '404.html')
        self.client.logout()
        request = self.client.get(reverse('todolist_edit', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_editView_post(self):
		self.client.login(username='user', password='1234')
		response = self.client.post(reverse('todolist_edit', args={1}))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'form.html')
		self.client.logout()
		response = self.client.post(reverse('todolist_edit', args={1}))
		self.assertEqual(response.status_code, 302)
		self.assertRedirects(
            response,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_deleteView_get(self):
        request = self.client.get(reverse('todolist_delete', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        request = self.client.get(reverse('todolist_delete', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_deleteView_post(self):
        response = self.client.post(reverse('todolist_delete', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        response = self.client.post(reverse('todolist_delete', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_switchView_get(self):
        request = self.client.get(reverse('todolist_done', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        request = self.client.get(reverse('todolist_done', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/login/',
            status_code=302,
            target_status_code=200)

    def test_switchView_post(self):
        response = self.client.post(reverse('todolist_done', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        response = self.client.post(reverse('todolist_done', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/login/',
            status_code=302,
            target_status_code=200)






