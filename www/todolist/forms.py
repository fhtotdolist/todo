from django import forms
from django.forms import ModelForm, Textarea
from www.todolist.models import TodoEntry


class EntryForm(forms.ModelForm):
    Description = forms.CharField(widget=forms.Textarea(attrs={'cols': 40, 'rows': 3}))
    class Meta:
        model = TodoEntry
        fields = ["Title", "Description", "Duedate", "Duetime"]
       # widgets = {
       #    'Description': Textarea(attrs={'cols': 40, 'rows': 3}),
       # }