from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class TodoEntry(models.Model):
	User = models.ForeignKey(User)
	Title = models.CharField(max_length=250, null=False)
	Description = models.CharField(max_length=250, null=True)
	Duedate = models.DateField(blank=True, null=True)
	Duetime = models.TimeField(blank=True, null=True)
	OPEN = 'OPEN'
	DONE = 'DONE'
	Status_Choices = (
		(OPEN, 'OPEN'),
		(DONE, 'DONE'),
	)
	Status = models.CharField(max_length=4, choices=Status_Choices, default=OPEN)
