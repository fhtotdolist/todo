# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def load_stores_from_fixture(apps, schema_editor):
	    from django.core.management import call_command
	    call_command("loaddata", "initial.json")

class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_todoentry_status'),
    ]

    operations = [
    	migrations.RunPython(load_stores_from_fixture),
    ]
