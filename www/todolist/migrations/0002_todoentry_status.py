# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='todoentry',
            name='Status',
            field=models.CharField(default=b'OPEN', max_length=4, choices=[(b'OPEN', b'OPEN'), (b'DONE', b'DONE')]),
        ),
    ]
