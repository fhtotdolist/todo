# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TodoEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Title', models.CharField(max_length=250)),
                ('Description', models.CharField(max_length=250, null=True)),
                ('Duedate', models.DateField(null=True, blank=True)),
                ('Duetime', models.TimeField(null=True, blank=True)),
                ('User', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
