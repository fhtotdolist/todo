module.exports = function(grunt) {
  // Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
    	dist: {
    		options: {
    			style: 'compressed'
    		},
    		files: {
    			'www/static/stylesheet/screen.css' : 'www/sass/screen.scss'
    		}
    	}
    },
    watch: {
    	css: {
    		files: '**/*scss',
    		tasks: ['sass']
    	}
    }
    // Define your tasks    

  });

  // Load packages needed for tasks
  // e.g. grunt.loadNpmTasks('PACKAGE');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  
  // Register Tasks
  // e.g. grunt.registerTask('default', ['watch']);
  grunt.registerTask('default', ['watch']);
};
